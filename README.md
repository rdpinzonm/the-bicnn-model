# README #

* This repository contains the implementation of the bi-hemispheric neuronal network model of the cerebellum presented in:

[Pinzon Morales RD and Hirata Y (2014)](http://journal.frontiersin.org/Journal/10.3389/fncir.2014.00131/abstract). A bi-hemispheric neuronal network model of the cerebellum with spontaneous climbing fiber firing produces asymmetrical motor learning during robot control. Front. Neural Circuits 8:131. doi: 10.3389/fncir.2014.00131

* Version 1.0
* The model is implemented in LabVIEW 2013 SP1. Other version will be available soon.
* Although the biCNN model is stable as the version 1.0, the GUI and more examples are under development. 

* To run this model a step-by-step manual is included in the Documentation Folder. The manual objective is to simulate the control of a DC motor with the biCNN model. 

LICENSE

This software is free for non-commercial use and modification, however:
THE PERMISSION THROUGH EMAIL IS REQUIRED.
Commercial use is possible on individually negotiated conditions.
In both cases, contact the author by email: yutaka@isc.chubu.ac.jp, rdpinzonm@ieee.org

﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="13008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Create CELLUM" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="TypeDefs" Type="Folder">
				<Item Name="Neuron Info.ctl" Type="VI" URL="../Controls/Neuron Info.ctl"/>
				<Item Name="SM Allocate Neurons.ctl" Type="VI" URL="../Controls/SM Allocate Neurons.ctl"/>
				<Item Name="Type of Neuron.ctl" Type="VI" URL="../Controls/Type of Neuron.ctl"/>
				<Item Name="Type of Synapses.ctl" Type="VI" URL="../Controls/Type of Synapses.ctl"/>
				<Item Name="Synapses.ctl" Type="VI" URL="../Controls/Synapses.ctl"/>
				<Item Name="Get Synapses per Neuron.vi" Type="VI" URL="../Utilities/Get Synapses per Neuron.vi"/>
				<Item Name="SM load Cere.ctl" Type="VI" URL="../Controls/SM load Cere.ctl"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Property Name="NI.SortType" Type="Int">0</Property>
				<Item Name="1DArrayTo2DArray size.vi" Type="VI" URL="../Utilities/1DArrayTo2DArray size.vi"/>
				<Item Name="AccNeurons.vi" Type="VI" URL="../Utilities/AccNeurons.vi"/>
				<Item Name="ActivationFunction.vi" Type="VI" URL="../Utilities/ActivationFunction.vi"/>
				<Item Name="AvailableElements.vi" Type="VI" URL="../Utilities/AvailableElements.vi"/>
				<Item Name="Camera Setup.vi" Type="VI" URL="../MotorDemo/Camera Setup.vi"/>
				<Item Name="coor2id.vi" Type="VI" URL="../Utilities/coor2id.vi"/>
				<Item Name="Delete First Element.vi" Type="VI" URL="../Utilities/Delete First Element.vi"/>
				<Item Name="Euclidean distance.vi" Type="VI" URL="../Utilities/Euclidean distance.vi"/>
				<Item Name="Generate IDs.vi" Type="VI" URL="../Utilities/Generate IDs.vi"/>
				<Item Name="Get Square Periphery.vi" Type="VI" URL="../Utilities/Get Square Periphery.vi"/>
				<Item Name="id2coor.vi" Type="VI" URL="../Utilities/id2coor.vi"/>
				<Item Name="Id2TypeColor.vi" Type="VI" URL="../Utilities/Id2TypeColor.vi"/>
				<Item Name="Init.vi" Type="VI" URL="../MotorDemo/3D world/Init.vi"/>
				<Item Name="IO nucleus.vi" Type="VI" URL="../MotorDemo/IO nucleus.vi"/>
				<Item Name="LiveGrid.vi" Type="VI" URL="../Cube/LiveGrid.vi"/>
				<Item Name="Mf generator.vi" Type="VI" URL="../MotorDemo/Mf generator.vi"/>
				<Item Name="Motor 3D Model.vi" Type="VI" URL="../MotorDemo/3D world/Motor 3D Model.vi"/>
				<Item Name="Motor TF.vi" Type="VI" URL="../MotorDemo/Motor TF.vi"/>
				<Item Name="PlotGrid.vi" Type="VI" URL="../Utilities/PlotGrid.vi"/>
				<Item Name="PlotScheme.vi" Type="VI" URL="../Utilities/PlotScheme.vi"/>
				<Item Name="ProgressBar.vi" Type="VI" URL="../Utilities/ProgressBar.vi"/>
				<Item Name="Rand W.vi" Type="VI" URL="../Utilities/Rand W.vi"/>
				<Item Name="Ref Signal.vi" Type="VI" URL="../MotorDemo/Ref Signal.vi"/>
				<Item Name="Rotate.vi" Type="VI" URL="../Cube/Rotate.vi"/>
				<Item Name="RSE.vi" Type="VI" URL="../Utilities/RSE.vi"/>
				<Item Name="Start Simulation.vi" Type="VI" URL="../Utilities/Start Simulation.vi"/>
				<Item Name="Synapse2neurons.vi" Type="VI" URL="../Utilities/Synapse2neurons.vi"/>
			</Item>
			<Item Name="Test Files" Type="Folder">
				<Item Name="Create Save Grid.vi" Type="VI" URL="../Test Files/Create Save Grid.vi"/>
				<Item Name="Create Save Plot Scheme.vi" Type="VI" URL="../Test Files/Create Save Plot Scheme.vi"/>
				<Item Name="cerebellum.vi" Type="VI" URL="../Test Files/cerebellum.vi"/>
				<Item Name="Load Grid Save Synp AER.vi" Type="VI" URL="../Test Files/Load Grid Save Synp AER.vi"/>
				<Item Name="Load Grid Plot AER.vi" Type="VI" URL="../Test Files/Load Grid Plot AER.vi"/>
				<Item Name="Create Save Plot Grid.vi" Type="VI" URL="../Test Files/Create Save Plot Grid.vi"/>
			</Item>
			<Item Name="DEMO" Type="Folder">
				<Item Name="DC Motor.vi" Type="VI" URL="../MotorDemo/3D world/DC Motor.vi"/>
				<Item Name="Quadcopter.vi" Type="VI" URL="../QuadDemo/Quadcopter.vi"/>
			</Item>
			<Item Name="Grid.lvclass" Type="LVClass" URL="../Cube/Grid.lvclass"/>
			<Item Name="Neuron.lvclass" Type="LVClass" URL="../Neuron/Neuron.lvclass"/>
			<Item Name="Synapses.lvclass" Type="LVClass" URL="../Synapses/Synapses.lvclass"/>
			<Item Name="AER.lvclass" Type="LVClass" URL="../AER/AER.lvclass"/>
			<Item Name="Cerebellum.lvclass" Type="LVClass" URL="../Cerebellum/Cerebellum.lvclass"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Draw Arc.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Arc.vi"/>
				<Item Name="Draw Circle by Radius.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Draw Circle by Radius.vi"/>
				<Item Name="Draw Line.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Line.vi"/>
				<Item Name="Draw Text at Point.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Text at Point.vi"/>
				<Item Name="Draw Text in Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Text in Rect.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="LV3DPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LV3DPointTypeDef.ctl"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRGBAColorTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRGBAColorTypeDef.ctl"/>
				<Item Name="LVTextureCoordinateArrayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVTextureCoordinateArrayTypeDef.ctl"/>
				<Item Name="Move Pen.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Move Pen.vi"/>
				<Item Name="NI_3D Picture Control.lvlib" Type="Library" URL="/&lt;vilib&gt;/picture/3D Picture Control/NI_3D Picture Control.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="NI_Vision_Development_Module.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/NI_Vision_Development_Module.lvlib"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PCT Pad String.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/PCT Pad String.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Set Pen State.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Set Pen State.vi"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="nivision.dll" Type="Document" URL="nivision.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
